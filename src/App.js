import { CssBaseline } from '@mui/material';
import Dashboard from './pages/Dashboard';

function App() {
  return (
    <>
    <CssBaseline />
    <Dashboard />
    </>

  );
}

export default App;
